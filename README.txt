Editor Absolute Image URL is a module that overrides the core EditorImageDialog and adds an option (checkbox) to output the image HTML with an absolute src URL.

There are use cases where this is desirable, such as in certain headless applications.